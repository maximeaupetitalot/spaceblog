<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Service\BlogService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\String\Slugger\AsciiSlugger;

use Doctrine\Persistence\ManagerRegistry;

class AdminController extends AbstractController
{
	/**
	 * @Route("/admin", name="admin-page")
	*/
	public function show(ManagerRegistry $manager): Response
	{
		$articles = (new BlogService($manager))->getAll();

		return $this->render('admin_page.html.twig', [ 'articles' => $articles ]);
	}
	
	/**
	 * @Route("/admin/delete/{slug}", name="delete-article")
	*/
	public function delete(ManagerRegistry $manager, string $slug): Response
	{
	    try {
	        $unslugged = explode('-', $slug);
            $id = intval($unslugged[0]);
            $article = (new BlogService($manager))->get(intval($id));

            if ($slug != $id . '-' . (new AsciiSlugger())->slug($article->getTitle()))
                return $this->redirectToRoute('home-page');

		    (new BlogService($manager))->delete($id);
		
		    return $this->redirectToRoute('admin-page');
		}
		catch (ErrorException $e)
        {
            return $this->redirectToRoute('home-page');
        }
	}
}

?>
