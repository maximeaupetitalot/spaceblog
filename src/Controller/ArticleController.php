<?php

namespace App\Controller;

use App\Entity\Article;
use App\Service\BlogService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

use Doctrine\Persistence\ManagerRegistry;

class ArticleController extends AbstractController
{
	/**
	 * @Route("/article/{slug}", name="article-page")
	*/
	public function show(ManagerRegistry $manager, string $slug): Response
	{
	    try {
	        $unslugged = explode('-', $slug);
	        $id = intval($unslugged[0]);
		    $article = (new BlogService($manager))->get($id);

		    if ($slug != $id . '-' . (new AsciiSlugger())->slug($article->getTitle()))
		        return $this->redirectToRoute('home-page');

		    setLocale(LC_TIME, 'fr_CA.UTF-8');

		    $creation_date = \DateTime::createFromFormat('Y-m-d H:i:s', $article->getCreationDate());
		    $article->setCreationDate($creation_date->format('\l\e d/m/Y à H:i'));
		
		    return $this->render('article_page.html.twig', [ 'article' => $article ]);
	    }
	    catch (ErrorException $e)
	    {
	        return $this->redirectToRoute('home-page');
	    }
	}
}

?>
