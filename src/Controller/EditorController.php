<?php

namespace App\Controller;

use App\Form\ArticleType;
use App\Entity\Article;
use App\Service\BlogService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

use Doctrine\Persistence\ManagerRegistry;

class EditorController extends AbstractController
{
    private function buildForm(?Article $old_article, Request $request, ?Form &$form, ManagerRegistry $manager, SluggerInterface $slugger): ?Response
	{
		$article = new Article();
		
		$form = $this->createForm(ArticleType::class, $article, [ 'data_class' => 'App\Entity\Article' ]);
		
		if ($old_article != null)
		{
			$form->add('title', TextType::class, [ 'data_class' => null,
				'label' => false,
				'data' => $old_article->getTitle(),
				'attr' => [ 'placeholder' => 'Titre de votre article' ]
			]);

			$content = $old_article->getContent();
			$content = str_replace("&nbsp;", " ", $content);
            $content = str_replace("<br />", "\n", $content);
			
			$form->add('content', TextareaType::class, [ 'data_class' => null,
				'label' => false,
				'data' => $content,
				'attr' => [ 'placeholder' => 'Rédigez votre article ici.' ]
			]);
		}
			
		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid())
		{
			$template = $form->getData();
			
			$article->setTitle($template->getTitle());
			
			$content = $template->getContent();
			$content = str_replace(" ", "&nbsp;", $content);
			$content = str_replace("\n", "<br />", $content);
			$article->setContent($content);
			
			$file = $form->get('cover')->getData();
			
			if ($file)
			{
				$fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
				$newFileName = uniqid() . '-' . $slugger->slug($fileName) . '.' . $file->guessExtension();
				
				try
				{
					$file->move($this->getParameter('images_dir'), $newFileName);
				}
				catch (FileException $e)
				{}
			}
			
			$article->setCover($newFileName);
			
			if ($old_article == null)
				(new BlogService($manager))->insert($article);
			else
				(new BlogService($manager))->update($old_article->getId(), $article);
			
			return $this->redirectToRoute('article-page', [ 'slug' =>
			    ($old_article == null ? $article->getId() : $old_article->getId())
			        . '-' . (new AsciiSlugger())->slug($article->getTitle()
			    )
			]);
		}
		
		return null;
	}
	
	/**
	 * @Route("/admin/new", "new-article")
	 */
	public function new_(ManagerRegistry $manager, Request $request, SluggerInterface $slugger): Response
	{
		$form = null;
		$res = $this->buildForm(null, $request, $form, $manager, $slugger);
		
		if ($res)
			return $res;
		
		return $this->renderForm('editor_page.html.twig', [ 'form' => $form ]);
	}
	
	/**
	 * @Route("/admin/edit/{slug}", "edit-article")
	 */
	public function edit(ManagerRegistry $manager, Request $request, SluggerInterface $slugger, string $slug): Response
	{
	    try {
            $unslugged = explode('-', $slug);
            $id = intval($unslugged[0]);
            $article = (new BlogService($manager))->get(intval($id));

            if ($slug != $id . '-' . (new AsciiSlugger())->slug($article->getTitle()))
                return $this->redirectToRoute('home-page');

		    $form = null;
		    $article = (new BlogService($manager))->get($id);
		    $res = $this->buildForm($article, $request, $form, $manager, $slugger);
		
		    if ($res)
			    return $res;
		
		    return $this->renderForm('editor_page.html.twig', [ 'form' => $form ]);
		}
		catch (ErrorException $e)
        {
            return $this->redirectToRoute('home-page');
        }
	}
}

?>
