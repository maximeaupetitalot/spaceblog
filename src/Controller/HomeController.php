<?php

namespace App\Controller;

use App\Entity\Article;
use App\Service\BlogService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;

use Doctrine\Persistence\ManagerRegistry;

class HomeController extends AbstractController
{
	/**
	 * @Route("/", name="home-page")
	*/
	public function show(ManagerRegistry $manager): Response
	{
		$articles = (new BlogService($manager))->getAll();

		return $this->render('home_page.html.twig', [ 'articles' => $articles ]);
	}
}

?>
