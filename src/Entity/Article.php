<?php

namespace App\Entity;

use App\Repository\ArticleRepository;

use Doctrine\ORM\Mapping as Record;

use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * @Record\Entity(repositoryClass=ArticleRepository::class)
*/
class Article
{
	/**
	 * @Record\Id()
	 * @Record\GeneratedValue()
	 * @Record\Column(type="integer")
	 */
	private $id;
	
	/**
	 * @Record\Column(type="string", length=255)
	 */
	private $title;
	
	/**
	 * @Record\Column(type="text")
	 */
	private $content;
	
	/**
	 * @Record\Column(type="string", length=255)
	 */
	private $cover;
	
	/**
	 * @Record\Column(type="string", length=19)
	 */
	private $creation_date;
	
	public function __construct()
	{
		$this->creation_date = (new \DateTime('now'))->format('Y-m-d H:i:s');
	}

	public function getId(): ?int
	{
		return $this->id;
	}
	
	public function getTitle(): ?string
	{
		return $this->title;
	}
	
	public function getContent(): ?string
	{
		return $this->content;
	}
	
	public function getCover(): ?string
	{
		return $this->cover;
	}
	
	public function getCreationDate(): ?string
	{
		return $this->creation_date;
	}

	public function getSlug(): string
    {
    	return $this->getId() . '-' . (new AsciiSlugger())->slug($this->getTitle());
    }
		
	public function setTitle(string $title)
	{
		$this->title = $title;
	}
	
	public function setContent(string $content)
	{
		$this->content = $content;
	}
	
	public function setCover(string $cover)
	{
		$this->cover = $cover;
	}
	
	public function setCreationDate(string $date)
	{
		$this->creation_date = $date;
	}
};

?>
