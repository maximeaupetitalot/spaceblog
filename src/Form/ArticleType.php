<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use App\Entity\Article;

class ArticleType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('cover', FileType::class, [
			'label' => 'Déposez une ouverture pour votre article (au format .gif, .png, .jpg, .bmp ou .webp)',
			'mapped' => false,
			'required' => false,
			'constraints' => [
				new File([
					'maxSize' => '4096k',
					'mimeTypes' => [
						'image/gif',
						'image/png',
						'image/jpeg',
						'image/bmp',
						'image/webp'
					],
					
					'mimeTypesMessage' => 'Le fichier doit être une image valide.',
				])
			],
		], [ 'data_class' => FileType::class ])
			->add('title', TextType::class, [ 'data_class' => TextType::class,
				'label' => false,
				'attr' => [ 'placeholder' => 'Titre de votre article' ] ])
			->add('content', TextareaType::class, [ 'data_class' => TextareaType::class,
				'label' => false,
				'attr' => [ 'placeholder' => 'Rédigez votre article ici.' ],
				'trim' => false ])
			->add('save', SubmitType::class, [
				'attr' => [ 'class' => 'main-button' ],
				'label' => 'Publier'
			]);
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\Entity\Article'
		]);
	}
}

?>
