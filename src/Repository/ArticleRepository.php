<?php

namespace App\Repository;

use App\Entity\Article;

use Doctrine\ORM\EntityRepository;

class ArticleRepository extends EntityRepository
{
    public function selectAllArticles(): array
    {
        return $this->getEntityManager()->createQuery(
            'SELECT article
             FROM App\Entity\Article article'
        )->getResult();
    }
    
    public function selectArticleById(int $id): Article
    {
        return $this->getEntityManager()->createQuery(
            'SELECT article
             FROM App\Entity\Article article
             WHERE article.id = :id'
        )->setParameter('id', $id)->getResult()[0];
    }

    public function insertArticle(Article $article)
    {
        $this->getEntityManager()->persist($article);
        $this->getEntityManager()->flush();
    }

    public function updateArticle(int $id, Article $article)
    {
        $target = $this->selectArticleById($id);

        if ($target == null)
            throw $this->createNotFoundException("Selected article not found");

        $target->setTitle($article->getTitle());
        $target->setContent($article->getContent());
        $target->setCover($article->getCover());

        $this->getEntityManager()->flush();
    }

    public function deleteArticle(Article $article)
    {
        $this->getEntityManager()->remove($article);
        $this->getEntityManager()->flush();
    }
};

?>
