<?php

namespace App\Service;

use App\Entity\Article;
use App\Repository\ArticleRepository;

use Doctrine\Persistence\ManagerRegistry;

class BlogService
{
	private $repository;
	
	public function __construct(ManagerRegistry $manager)
	{
		$this->repository = $manager->getManager()->getRepository(Article::class);
	}
	
	public function getAll(): array
	{
		return $this->repository->selectAllArticles();
	}
	
	public function get(int $id): Article
	{
		return $this->repository->selectArticleById($id);
	}
	
	public function insert(Article $article)
	{
		$this->repository->insertArticle($article);
	}
	
	public function update(int $id, Article $article)
	{
		$this->repository->updateArticle($id, $article);
	}
	
	public function delete(int $id)
	{
		$article = $this->get($id);
		
		return $this->repository->deleteArticle($article);
	}
}

?>
